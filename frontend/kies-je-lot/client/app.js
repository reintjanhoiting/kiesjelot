import React from 'react'
import Blog from './components/blog'

import 'bootstrap-material-design'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

import 'bootstrap-material-design/dist/css/bootstrap-material-design.css'
import 'bootstrap-material-design/dist/css/ripples.css'
import 'styles/material-blog.css'
// import 'styles/styles.scss'
import './scripts/main'

export default () => (
  <div>
    <Blog />
  </div>)
