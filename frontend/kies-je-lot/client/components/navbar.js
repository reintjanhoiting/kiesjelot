import React from 'react'

const NavBar = (props) => (
  <div className='navbar navbar-material-blog navbar-success navbar-absolute-top navbar-overlay'>

    <div className='navbar-image' style={{backgroundImage: 'url(img/food/unsplash-4.jpg)', backgroundPosition: 'center 40%'}} />

    <div className='navbar-wrapper container'>
      <div className='navbar-header'>
        <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
          <span className='icon-bar' />
          <span className='icon-bar' />
          <span className='icon-bar' />
        </button>
        <a className='navbar-brand' href='index.html'><i className='material-icons'>&#xE871;</i> Material Blog</a>
      </div>
      <div className='navbar-collapse collapse navbar-responsive-collapse'>
        <ul className='nav navbar-nav'>
          <li className='active dropdown'>
            <a href='bootstrap-elements.html' data-target='#' className='dropdown-toggle' data-toggle='dropdown'>Stories <b className='caret' /></a>
            <ul className='dropdown-menu'>
              <li><a href='home-fashion.html'>Fashion</a></li>
              <li><a href='home-food.html'>Food</a></li>
              <li><a href='home-music.html'>Music</a></li>
              <li><a href='home-photography.html'>Photography</a></li>
              <li><a href='home-technology.html'>Technology</a></li>
              <li><a href='home-travel.html'>Travel</a></li>
            </ul>
          </li>
          <li className='dropdown'>
            <a href='bootstrap-elements.html' data-target='#' className='dropdown-toggle' data-toggle='dropdown'>Filters <b className='caret' /></a>
            <ul className='dropdown-menu'>
              <li><a href='filter-category.html'>Category</a></li>
              <li><a href='filter-author.html'>Author</a></li>
              <li><a href='filter-date.html'>Date</a></li>
            </ul>
          </li>
          <li className='dropdown'>
            <a href='bootstrap-elements.html' data-target='#' className='dropdown-toggle' data-toggle='dropdown'>Post <b className='caret' /></a>
            <ul className='dropdown-menu'>
              <li><a href='post-image.html'>Image post</a></li>
              <li><a href='post-video.html'>Video post</a></li>
            </ul>
          </li>
          <li><a href='page-about.html'>About</a></li>
          <li><a href='page-contact.html'>Contact</a></li>
          <li className='dropdown hidden-sm'>
            <a href='bootstrap-elements.html' data-target='#' className='dropdown-toggle' data-toggle='dropdown'>Documentation <b className='caret' /></a>
            <ul className='dropdown-menu'>
              <li><a href='doc-buttons.html'>Buttons</a></li>
              <li><a href='doc-forms.html'>Forms</a></li>
              <li><a href='doc-icons.html'>Icons</a></li>
              <li><a href='doc-indicators.html'>Indicators</a></li>
              <li><a href='doc-navbars.html'>Navbars</a></li>
              <li><a href='doc-panels.html'>Panels</a></li>
              <li><a href='doc-tables.html'>Tables</a></li>
              <li><a href='doc-typography.html'>Typography</a></li>
            </ul>
          </li>
        </ul>
        <ul className='nav navbar-nav navbar-right'>
          <li><a href='#'><i className='fa fa-facebook' /></a></li>
          <li><a href='#'><i className='fa fa-google-plus' /></a></li>
          <li><a href='#'><i className='fa fa-twitter' /></a></li>
        </ul>
      </div>
    </div>
  </div>
)

export default NavBar
