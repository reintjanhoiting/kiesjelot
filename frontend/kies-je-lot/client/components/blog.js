import React from 'react'
import NavBar from './navbar'
const Blog = (props) => (
  <div>
    <NavBar />
    <div className='container blog-content'>
      <div className='row'>
        <div className='col-sm-8 blog-main'>
          <div className='row'>
            <div className='col-sm-12'>
              <section className='blog-post'>
                <div className='panel panel-default'>
                  <img src='img/travel/unsplash-2.jpg' className='img-responsive' />
                  <div className='panel-body'>
                    <div className='blog-post-meta'>
                      <span className='label label-light label-success'>Australia</span>
                      <p className='blog-post-date pull-right'>February 16, 2016</p>
                    </div>
                    <div className='blog-post-content'>
                      <a href='post-image.html'>
                        <h2 className='blog-post-title'>Like a little drop of ink</h2>
                      </a>
                      <p>Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem.</p>
                      <a className='btn btn-info' href='post-image.html'>Read more</a>
                      <a className='blog-post-share pull-right' href='#'>
                        <i className='material-icons'>&#xE80D;</i>
                      </a>
                    </div>
                  </div>
                </div>
              </section>
              <section className='blog-post'>
                <div className='panel panel-default'>
                  <img src='img/travel/unsplash-3.jpg' className='img-responsive' />
                  <div className='panel-body'>
                    <div className='blog-post-meta'>
                      <span className='label label-light label-success'>Australia</span>
                      <p className='blog-post-date pull-right'>Januari 9, 2016</p>
                    </div>
                    <div className='blog-post-content'>
                      <a href='post-image.html'>
                        <h2 className='blog-post-title'>I have arms for them</h2>
                      </a>
                      <p>Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem.</p>
                      <a className='btn btn-info' href='post-image.html'>Read more</a>
                      <a className='blog-post-share pull-right' href='#'>
                        <i className='material-icons'>&#xE80D;</i>
                      </a>
                    </div>
                  </div>
                </div>
              </section>
              <section className='blog-post'>
                <div className='panel panel-default'>
                  <div className='panel-body'>
                    <div className='blog-post-meta'>
                      <span className='label label-light label-primary'>Tips</span>
                      <p className='blog-post-date pull-right'>Februari 11, 2016</p>
                    </div>
                    <div className='blog-post-content'>
                      <a href='post-image.html'>
                        <h2 className='blog-post-title'>In a glass of water</h2>
                      </a>
                      <p>Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem.</p>
                      <p>Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra.</p>
                      <a className='btn btn-info' href='post-image.html'>Read more</a>
                      <a className='blog-post-share pull-right' href='#'>
                        <i className='material-icons'>&#xE80D;</i>
                      </a>
                    </div>
                  </div>
                </div>
              </section>
              <section className='blog-post'>
                <div className='panel panel-default'>
                  <img src='img/travel/unsplash-4.jpg' className='img-responsive' />
                  <div className='panel-body'>
                    <div className='blog-post-meta'>
                      <span className='label label-light label-success'>Australia</span>
                      <p className='blog-post-date pull-right'>January 24, 2016</p>
                    </div>
                    <div className='blog-post-content'>
                      <a href='post-image.html'>
                        <h2 className='blog-post-title'>I glide and swan</h2>
                      </a>
                      <p>Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem.</p>
                      <a className='btn btn-info' href='post-image.html'>Read more</a>
                      <a className='blog-post-share pull-right' href='#'>
                        <i className='material-icons'>&#xE80D;</i>
                      </a>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <nav>
            <ul className='pager'>
              <li><a className='withripple' href='#'>Previous</a></li>
              <li><a className='withripple' href='#'>Next</a></li>
            </ul>
          </nav>
        </div>
        <div className='col-sm-3 col-sm-offset-1 blog-sidebar'>
          <div className='sidebar-module'>
            <div className='form-group'>
              <input type='text' className='form-control' placeholder='Search' />
            </div>
          </div>
          <div className='sidebar-module'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h4>About</h4>
                <p>Donec ut libero sed arcu vehicula ultricies a non tortor. <em>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</em> Aenean ut gravida lorem.</p>
              </div>
            </div>
          </div>

          <div className='sidebar-module'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h4>Categories</h4>
                <ol className='categories list-unstyled'>
                  <li>
                    <a className='label label-light label-success' href='filter-category.html'>Australia</a>
                    <span className='label label-light label-default pull-right'>8</span>
                  </li>
                  <li>
                    <a className='label label-light label-warning' href='filter-category.html'>Europe</a>
                    <span className='label label-light label-default pull-right'>5</span>
                  </li>
                  <li>
                    <a className='label label-light label-primary' href='filter-category.html'>Tips</a>
                    <span className='label label-light label-default pull-right'>9</span>
                  </li>
                </ol>
              </div>
            </div>
          </div>

          <div className='sidebar-module'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h4>Archives</h4>
                <ol className='list-unstyled'>
                  <li><a href='filter-date.html'>February 2016</a></li>
                  <li><a href='filter-date.html'>January 2016</a></li>
                  <li><a href='filter-date.html'>December 2015</a></li>
                  <li><a href='filter-date.html'>November 2015</a></li>
                  <li><a href='filter-date.html'>October 2015</a></li>
                  <li><a href='filter-date.html'>September 2015</a></li>
                  <li><a href='filter-date.html'>August 2015</a></li>
                  <li><a href='filter-date.html'>July 2015</a></li>
                  <li><a href='filter-date.html'>June 2015</a></li>
                  <li><a href='filter-date.html'>May 2015</a></li>
                  <li><a href='filter-date.html'>April 2015</a></li>
                  <li><a href='filter-date.html'>March 2015</a></li>
                </ol>
              </div>
            </div>
          </div>

          <div className='sidebar-module'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h4>Newsletter</h4>
                <div className='form-group label-floating'>
                  <label className='control-label' htmlFor='name'>Name</label>
                  <input type='text' id='name' className='form-control input-sm' />
                </div>
                <div className='form-group label-floating'>
                  <label className='control-label' htmlFor='email'>E-mail</label>
                  <input type='text' id='email' className='form-control input-sm' />
                </div>
                <a href='#' className='btn btn-default btn-raised btn-sm btn-block'>Subscribe</a>
              </div>
            </div>
          </div>

          <div className='sidebar-module'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h4>Elsewhere</h4>
                <ol className='list-unstyled'>
                  <li><a href='#'>Facebook</a></li>
                  <li><a href='#'>Google+</a></li>
                  <li><a href='#'>Twitter</a></li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Blog
